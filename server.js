const express = require('express');
const app = express();
const bodyParser = require("body-parser");
const mysql = require('mysql');
const path = require('path');

const PORT = process.env.PORT || 3000;

var db = mysql.createPool({
    host     : 'us-cdbr-east-02.cleardb.com',
    user     : 'b330a406c3b428',
    password : 'b324c7e1',
    database : 'heroku_8531bd5b04e48a6'
});

app.use(function (req, res, next) {  
    res.header("Access-Control-Allow-Origin", "*");  
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");  
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');  
    next();  
});

app.use(bodyParser.urlencoded({extended : true }));

app.use(bodyParser.json())

var BackEnd = {
    get:{
        login: function (req, res) {
            var username = "'"+req.params.username+"'";
            var password = "'"+req.params.password+"'";
            var query = "select * from user where username ="+username+" and password ="+password+" Limit 1";
  
            db.query(query,function(error, dataJson){
                if (dataJson.length > 0) {
                    res.json({"err_code":0,"data":dataJson});
                }
                else if(dataJson.length <= 0) {
                    res.json({"err_code":1,"data":'Invalid Username and Password'});
                }
                
                if (error) {
                    res.json({"err_code": 2, "err_msg":error, "function": "login"});
                }
            });
        },
        getUserByRole: function(req, res) {
            var role = req.params.role;
            var query = "select * from user where role ="+role;

            db.query(query, function(error, dataJson) {
                res.json({"err_code":0, "data":dataJson});

                if(error) {
                    res.json({"err_code":1, "err_msg":error, "function": "getUserByRole"});
                }
            })
        },
        getRoutineByUsername: function(req, res) {
            var username = "'"+req.params.username+"'";
            var x = new Date();
            var dd = x.getDate();
            var mm = x.getMonth()+1;
            var yy = x.getFullYear();
            var dateNow = "'"+dd +"-" + mm+"-" + yy+"'";

            var query = "select id, task_name, updated_by, DATE_FORMAT(date, '%d %M %Y') as date, if(routine_status = 1, 'done', 'pending') as status, lokasi, notes from routine where updated_by = "+username+" and DATE_FORMAT(date, '%d-%m-%Y') = "+dateNow;
            db.query(query, function(error, dataJson) {
                res.json({"err_code":0, "data":dataJson});

                if(error) {
                    res.json({"err_code":1, "err_msg":error, "function": "getRoutineByUsername"});
                }
            })
        },
        getRoutineByUserDate: function(req, res) {
            var username = "'"+req.params.username+"'";
            var dateNow = "'"+req.params.date+"'";

            var query = "select id, task_name, updated_by, DATE_FORMAT(date, '%d %M %Y') as date, if(routine_status = 1, 'done', 'pending') as status, lokasi, notes from routine where updated_by = "+username+" and DATE_FORMAT(date, '%d-%m-%Y') = "+dateNow;
            db.query(query, function(error, dataJson) {
                res.json({"err_code":0, "data":dataJson});

                if(error) {
                    res.json({"err_code":1, "err_msg":error, "function": "getRoutineByUserDate"});
                }
            })
        },
        getProjectByStartDate: function(req, res) {
            var startdate1 = "'"+req.params.startdate1+"'";
            var startdate2 = "'"+req.params.startdate2+"'";
            var query = "select id, title, owner, staff, DATE_FORMAT(start_date, '%d %M %Y') as start_date, DATE_FORMAT(end_date, '%d %M %Y') as end_date, if(status = 1, 'done', 'onprogress') as status, DATE_FORMAT(realize_date, '%d %M %Y') as realize_date, notes from task where start_date between STR_TO_DATE("+startdate1+", '%d-%m-%Y') and STR_TO_DATE("+startdate2+", '%d-%m-%Y')";
            
            db.query(query, function(error, dataJson) {
                res.json({"err_code":0, "data":dataJson});

                if(error) {
                    res.json({"err_code":1, "err_msg":error, "function": "getRoutineByUsername"});
                }
            })
        },
        getProjectByUserStatus: function(req, res) {
            var username = "'"+req.params.username+"'";
            var status = req.params.status;
            var query = "select id, title, owner, staff, DATE_FORMAT(start_date, '%d %M %Y') as start_date, DATE_FORMAT(end_date, '%d %M %Y') as end_date, if(status = 1, 'done', 'onprogress') as status, DATE_FORMAT(realize_date, '%d %M %Y') as realize_date, notes, lokasi from task where staff = "+username+" and status = "+status;
            
            db.query(query, function(error, dataJson) {
                res.json({"err_code":0, "data":dataJson});

                if(error) {
                    res.json({"err_code":1, "err_msg":error, "function": "getProjectByUserStatus"});
                }
            })
        },
        getDetailProject: function(req, res) {
            var id = req.params.id;
            var query = "select id, title, owner, staff, DATE_FORMAT(start_date, '%d %M %Y') as start_date, DATE_FORMAT(end_date, '%d %M %Y') as end_date, if(status = 1, 'done', 'onprogress') as status, DATE_FORMAT(realize_date, '%d %M %Y') as realize_date, notes from task where id="+id;
            
            db.query(query, function(error, dataJson) {
                res.json({"err_code":0, "data":dataJson});

                if(error) {
                    res.json({"err_code":1, "err_msg":error, "function": "getDetailProject"});
                }
            })
        }
    },
    post:{
        createProject: function(req, res) {
            var title = "'"+req.params.title+"'";
            var owner = "'"+req.params.owner+"'";
            var staff = "'"+req.params.staff+"'";
            var start_date = "'"+req.params.start_date+"'";
            var end_date = "'"+req.params.end_date+"'";
            var notes = "'"+req.params.notes+"'";
            var status = 0
            var lokasi = "'"+req.params.lokasi+"'";

            var query = "insert into task (title, owner, staff, start_date, end_date, notes, status, lokasi) values ("+title+", "+owner+", "+staff+", STR_TO_DATE("+start_date+", '%d-%m-%Y'), STR_TO_DATE("+end_date+", '%d-%m-%Y'), "+notes+", "+status+", "+lokasi+")";
 
            db.query(query,function(error, dataJson){

                var query1 = "SELECT LAST_INSERT_ID()"
                db.query(query1,function(error, dataJson){
                    
                    res.json({});

                    if (error) {
                        res.json({"err_code": 2, "err_msg":error, "function": "createProject"});
                    }
                });
                if (error) {
                    res.json({"err_code": 1, "err_msg":error, "function": "createProject"});
                }
            });
        },
        createRoutine: function(req, res) {
            var task_name = "'"+req.params.task_name+"'";
            var updated_by = "'"+req.params.username+"'";
            var routine_status = 0;
            var lokasi = "'"+req.params.lokasi+"'";
            var x = new Date();
            var dd = x.getDate();
            var mm = x.getMonth()+1;
            var yy = x.getFullYear();
            var date = "'"+dd +"-" + mm+"-" + yy+"'";

            var query = "insert into routine (task_name, updated_by, date, routine_status, lokasi) values ("+task_name+", "+updated_by+", STR_TO_DATE("+date+", '%d-%m-%Y'), "+routine_status+", "+lokasi+")";
 
            db.query(query,function(error, dataJson){

                var query1 = "SELECT LAST_INSERT_ID()"
                db.query(query1,function(error, dataJson){
                    
                    res.json({});

                    if (error) {
                        res.json({"err_code": 2, "err_msg":error, "function": "createRoutine"});
                    }
                });
                if (error) {
                    res.json({"err_code": 1, "err_msg":error, "function": "createRoutine"});
                }
            });
        }
    },
    put:{
        updateRoutine: function(req, res) {
            var id = req.params.id;
            var status = req.params.status;
            var notes = "'"+req.params.notes+"'"
            var query = "UPDATE routine SET routine_status = "+status+", notes = "+notes+" WHERE routine.id = "+id;
 
            db.query(query,function(error, dataJson){    
                res.json({});

                if (error) {
                    res.json({"err_code": 1, "err_msg":error, "function": "updateRoutine"});
                }
            });
        },
        updateproject:function(req, res) {
            var id = req.params.id;
            var status = req.params.status;
            var notes = "'"+req.params.notes+"'";
            var x = new Date();
            var dd = x.getDate();
            var mm = x.getMonth()+1;
            var yy = x.getFullYear();
            var date = "'"+dd +"-" + mm+"-" + yy+"'";

            var query = "UPDATE task SET status = "+status+", notes = "+notes+", realize_date = STR_TO_DATE("+date+", '%d-%m-%Y') WHERE task.id = "+id;
 
            db.query(query,function(error, dataJson){    
                res.json({});

                if (error) {
                    res.json({"err_code": 1, "err_msg":error, "function": "updateproject"});
                }
            });
        },
        changePassword:function(req, res) {
            var username = "'"+req.params.username+"'";
            var oldPassword = "'"+req.params.oldPassword+"'";
            var newPassword = "'"+req.params.newPassword+"'";
            var query = "select * from user where username = "+username+" and password = "+oldPassword;
 
            db.query(query,function(error, dataJson){    
                if(dataJson.length > 0) {
                    let query1 = "update user set password = "+newPassword+ " where username = "+username;

                    db.query(query1, function(error, dataResult) {
                        res.json({"err_code": 0});

                        if (error) {
                            res.json({"err_code": 3, "err_msg":error, "function": "changePassword"});
                        }
                    });
                }
                else if(dataJson.length <= 0) {
                    res.json({"err_code": 2});
                }

                if (error) {
                    res.json({"err_code": 1, "err_msg":error, "function": "changePassword"});
                }
            });
        },
    },
    delete:{}
}

//API Listss
app.get('/login/:username/:password', BackEnd.get.login);
app.get('/getUserByRole/:role', BackEnd.get.getUserByRole);
app.get('/getRoutineByUsername/:username', BackEnd.get.getRoutineByUsername);
app.get('/getRoutineByUserDate/:username/:date', BackEnd.get.getRoutineByUserDate);
app.get('/getProjectByStartDate/:startdate1/:startdate2', BackEnd.get.getProjectByStartDate);
app.get('/getProjectByUserStatus/:username/:status', BackEnd.get.getProjectByUserStatus);
app.get('/getDetailProject/:id', BackEnd.get.getDetailProject);

app.get('/createProject/:title/:owner/:staff/:start_date/:end_date/:notes/:lokasi', BackEnd.post.createProject);
app.get('/createRoutine/:task_name/:username/:lokasi', BackEnd.post.createRoutine);

app.put('/updateRoutine/:id/:status/:notes', BackEnd.put.updateRoutine);
app.put('/updateproject/:id/:status/:notes', BackEnd.put.updateproject);
app.put('/changePassword/:username/:oldPassword/:newPassword', BackEnd.put.changePassword);


app.use(express.static(path.join(__dirname, 'public')));

app.use(express.static(__dirname + '/public'));

app.get('/',function(req,res){
    res.sendFile(path.join(__dirname+'/public/index.html'));
})

app.listen(PORT, () => {
    console.log('Server Running at %s', PORT);
});